import React, { Component } from 'react';
import scan from '../../assets/images/scan.svg';
import edit from "../../assets/images/edit.svg";
import checkin from "../../assets/images/check_in_01.svg";

class OrderShipment extends Component {
    render() {
        return (
            <div className="maincontent">
            <div className="row m-0 p-0">
              
                <div className="col-12 col-sm-3 p-l-0">
                  <div className="form-group fm-height">
                  <label>Scan Barcode</label>
                <input
                  type="search"                  
                  className="form-control frm-pr"
                  placeholder="Scan Barcode"
                />
                          <button type="button" className="scan">
                            <img src={scan} /> SCAN
                          </button>
                  </div>                   
                </div>
                <div className='col-12 col-sm-2'>
                    <div className="form-group ">
                        <label>Available Qty</label>
                        	<input type='number' className='form-control' placeholder='200'></input>
                    </div>
                </div>
                <div className='col-12 col-sm-2'>
                    <div className="form-group ">
                        <label>Qty</label>
                        	<input type='number' className='form-control' placeholder='0'></input>
                    </div>
                </div>
                <div className='col-sm-3 mt-4'>
                <button className="btn-unic-search active">Search</button>
                </div>
                <div className="col-12 col-sm-4 scaling-center p-l-0">
                  <h5 className="fs-18 mt-3 mb-2">
                    Order Shipments
                  </h5>
                </div>
                <div className='row'>
                <div className="table-responsive p-0">
                  <table className="table table-borderless mb-1">
                    <thead>
                      <tr className="">
                        <th className="col-1">S.NO</th>
                        <th className="col-2">BARCODE</th>
                        <th className="col-1">FROM STORE</th>
                        <th className="col-1">TO STORE</th>
                        <th className="col-1">QTY</th>
                        <th className="col-2">TRANSFERRED DATE</th>
                        <th className="col-1">STATUS</th>
                        <th className="col-3">DESCRIPTION</th>
                      </tr>
                    </thead>
                    <tbody>
                         <tr>
                            <td className='col-1'>01</td>
                            <td className='col-2'>BC123456789</td>
                            <td className='col-1'>KPHB Store1</td>
                            <td className='col-1'>KPHB Store2</td>
                            <td className='col-1'>10000</td>
                            <td className='col-2'>30 Sep 2021</td>
                            <td className='col-1'>In-Transit</td>
                            <td className='col-3'>In publishing and graphic design, Lorem ipsum</td>
                           

                         </tr>  
                         <tr>
                            <td className='col-1'>01</td>
                            <td className='col-2'>BC123456789</td>
                            <td className='col-1'>KPHB Store1</td>
                            <td className='col-1'>KPHB Store2</td>
                            <td className='col-1'>10000</td>
                            <td className='col-2'>30 Sep 2021</td>
                            <td className='col-1'>In-Transit</td>
                            <td className='col-3'>In publishing and graphic design, Lorem ipsum</td>
                           

                         </tr>  
                         <tr>
                            <td className='col-1'>01</td>
                            <td className='col-2'>BC123456789</td>
                            <td className='col-1'>KPHB Store1</td>
                            <td className='col-1'>KPHB Store2</td>
                            <td className='col-1'>10000</td>
                            <td className='col-2'>30 Sep 2021</td>
                            <td className='col-1'>In-Transit</td>
                            <td className='col-3'>In publishing and graphic design, Lorem ipsum</td>
                           

                         </tr>   
                         <tr>
                            <td className='col-1'>01</td>
                            <td className='col-2'>BC123456789</td>
                            <td className='col-1'>KPHB Store1</td>
                            <td className='col-1'>KPHB Store2</td>
                            <td className='col-1'>10000</td>
                            <td className='col-2'>30 Sep 2021</td>
                            <td className='col-1'>In-Transit</td>
                            <td className='col-3'>In publishing and graphic design, Lorem ipsum</td>
                           

                         </tr>   
                         <tr>
                            <td className='col-1'>01</td>
                            <td className='col-2'>BC123456789</td>
                            <td className='col-1'>KPHB Store1</td>
                            <td className='col-1'>KPHB Store2</td>
                            <td className='col-1'>10000</td>
                            <td className='col-2'>30 Sep 2021</td>
                            <td className='col-1'>In-Transit</td>
                            <td className='col-3'>In publishing and graphic design, Lorem ipsum</td>
                           

                         </tr>   
                    </tbody>
                  </table>
                </div>
                </div>
            </div>
        </div>
        );
    }
}

export default OrderShipment;